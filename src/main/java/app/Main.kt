package app

import app.gui.config.SceneType
import javafx.application.Application
import javafx.stage.Stage
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ConfigurableApplicationContext
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene

/**
 * Main application
 */
@SpringBootApplication
open class Main : Application() {

    private var springContext: ConfigurableApplicationContext? = null
    private lateinit var root: Parent

    @Throws(Exception::class)
    override fun init() {
        springContext = SpringApplication.run(Main::class.java)
        val fxmlLoader = FXMLLoader(javaClass.classLoader.getResource(SceneType.LOGIN.value))
        fxmlLoader.setControllerFactory { springContext!!.getBean(it) }
        root = fxmlLoader.load()
    }

    @Throws(Exception::class)
    override fun start(primaryStage: Stage) {
        val scene = Scene(root, 800.0, 600.0)
        primaryStage.scene = scene
        primaryStage.show()

    }

    @Throws(Exception::class)
    override fun stop() {
        springContext!!.stop()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Application.launch(Main::class.java, *args)
        }
    }
}
