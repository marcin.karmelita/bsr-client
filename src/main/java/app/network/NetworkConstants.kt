package app.network

/**
 * Object defining constant values
 */
object NetworkConstants {
    /**
     * Server URI
     */
    val SERVER_URI = "http://localhost:8082/ws/clients"
    /**
     * Endpoint URI
     */
    val ENDPOINT = "http://localhost:8082/ws/"
}
