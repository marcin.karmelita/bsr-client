package app.network

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.support.BasicAuthorizationInterceptor
import org.springframework.oxm.jaxb.Jaxb2Marshaller
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor
import org.springframework.ws.transport.http.HttpComponentsMessageSender
import org.springframework.ws.client.core.WebServiceTemplate

/**
 * SOAP Client configuration
 */
@Configuration
open class NetworkConfiguration {

    /**
     * Marshaller bean for the NetworkClient
     * @return Jaxb2Marshaller
     */
    @Bean
    open fun marshaller(): Jaxb2Marshaller {
        val marshaller = Jaxb2Marshaller()
        marshaller.contextPath = "bds.wsdl"
        return marshaller
    }

    /**
     * NetworkClient bean
     * @param marshaller Jaxb2Marshaller
     *
     * @return NetworkClient
     */
    @Bean
    open fun client(marshaller: Jaxb2Marshaller): NetworkClient {
        val networkClient = NetworkClient()
        networkClient.defaultUri = NetworkConstants.SERVER_URI
        networkClient.marshaller = marshaller
        networkClient.unmarshaller = marshaller
        networkClient.interceptors = arrayOf(securityInterceptor())

        return networkClient
    }

    /**
     * Basic authorization interceptor
     * @return BasicAuthorizationInterceptor
     */
    @Bean
    open fun basicAuthInterceptor(): BasicAuthorizationInterceptor {
        return BasicAuthorizationInterceptor("test", "test")
    }

    /**
     * Security interceptor
     * @return Wss4jSecurityInterceptor
     */
    @Bean
    open fun securityInterceptor(): Wss4jSecurityInterceptor {
        val wss4jSecurityInterceptor = Wss4jSecurityInterceptor()
        wss4jSecurityInterceptor.setSecurementActions("Timestamp UsernameToken")
        wss4jSecurityInterceptor.setSecurementUsername("admin")
        wss4jSecurityInterceptor.setSecurementPassword("secret")
        return wss4jSecurityInterceptor
    }
}