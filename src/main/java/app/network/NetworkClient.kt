package app.network

import bds.wsdl.*
import org.springframework.stereotype.Component
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.SoapFaultClientException
import org.springframework.ws.soap.client.core.SoapActionCallback

/**
 * Network SOAP client
 */
class NetworkClient: WebServiceGatewaySupport() {

    /**
     * Get generic response
     * @param request generic request
     * @param endpoint endpoint
     * @return generic Response object
     *
     * @throws SoapFaultClientException
     */
    @Throws(SoapFaultClientException::class)
    private fun <Request, Response> getResponse(request: Request, endpoint: String): Response {
        val response: Response = webServiceTemplate
                .marshalSendAndReceive(NetworkConstants.SERVER_URI, request, SoapActionCallback(NetworkConstants.ENDPOINT + endpoint)) as Response
        return response
    }

    /**
     * Get accounts operation for the selected owner identifier
     * @param ownerId owner identifier
     *
     * @return list of Account objects
     */
    fun getAccounts(ownerId: String): List<Account> {
        val request = GetAccountsRequest()
        request.ownerId= ownerId
        val response = getResponse<GetAccountsRequest, GetAccountsResponse>(request, "getAccounts")
        return response.accounts
    }

    /**
     * Add account operation for the selected owner identifier
     * @param ownerId owner identifier
     *
     * @return Account object
     */
    fun addAccount(ownerId: String): Account {
        val request = AddAccountRequest()
        request.ownerId = ownerId
        val response = getResponse<AddAccountRequest, AddAccountResponse>(request, "addAccount")
        return response.account
    }

    /**
     * Get transactions operation for the selected account number
     * @param sourceAccountNumber account number
     *
     * @return list of Transaction objects
     */
    fun getTransactions(sourceAccountNumber: String): List<Transaction> {
        val request = GetTransactionsRequest()
        request.accountId = sourceAccountNumber
        val response = getResponse<GetTransactionsRequest, GetTransactionsResponse>(request, "getAccounts")
        return response.transactions
    }

    /**
     * Perform login operation
     * @param login username and password for the selected client
     *
     * @return Client object
     */
    fun login(login: Login): Client {
        val request = LoginRequest()

        request.login = login
        val response = getResponse<LoginRequest, LoginResponse>(request, "loginRequest")
        return response.client
    }

    /**
     * Perform transfer operation for the selected account number
     * @param transferModel transferModel
     *
     * @return Transaction object
     */
    fun transfer(transferModel: TransferModel): Transaction {
        val request = TransferRequest()
        request.transferModel = transferModel
        val response = getResponse<TransferRequest, TransferResponse>(request, "addTransaction")
        return response.transaction
    }

    /**
     * Perform register operation
     * @param login username and password for the selected client
     *
     * @return Client object
     */
    fun register(login: Login): Client {
        val request = RegisterClientRequest()

        request.login = login
        val response = getResponse<RegisterClientRequest, RegisterClientResponse>(request, "registerClientRequest")
        return response.client
    }

    /**
     * Perform top-up operation for the selected account number
     * @param cashModel CashModel
     *
     * @return Transaction object
     */
    fun topUp(cashModel: CashModel): Transaction {
        val request = TopUpRequest()
        request.cashModel = cashModel
        val response = getResponse<TopUpRequest, TopUpResponse>(request, "topUpRequest")
        return response.transaction
    }

    /**
     * Perform cash withdrawal operation for the selected account number
     * @param cashModel CashModel
     *
     * @return Transaction object
     */
    fun cashWithdrawal(cashModel: CashModel): Transaction {
        val request = CashWithdrawalRequest()
        request.cashModel = cashModel
        val response = getResponse<CashWithdrawalRequest, CashWithdrawalResponse>(request, "cashWithdrawalRequest")
        return response.transaction
    }
}