package app.gui.dashboard.history

import app.gui.utils.convertToSaldo
import app.gui.utils.name
import bds.wsdl.Transaction
import bds.wsdl.TransactionType
import javafx.fxml.FXML
import javafx.scene.control.Label

/**
 * Single history item controller
 */
open class HistoryItemController {

    @FXML private lateinit var titleLabel: Label
    @FXML private lateinit var transactionTypeLabel: Label
    @FXML private lateinit var amountLabel: Label
    @FXML private lateinit var balanceLabel: Label
    @FXML private lateinit var desintationNameLabel: Label
    @FXML private lateinit var desintationAccountLabel: Label

    @FXML private lateinit var titleLabelPrompt: Label
    @FXML private lateinit var transactionTypeLabelPrompt: Label
    @FXML private lateinit var amountLabelPrompt: Label
    @FXML private lateinit var balanceLabelPrompt: Label
    @FXML private lateinit var desintationNameLabelPrompt: Label
    @FXML private lateinit var desintationAccountLabelPrompt: Label


    /**
     * Setup view with the item (transaction)
     * @param transaction item
     */
    fun setup(transaction: Transaction) {
        titleLabel.text = transaction.title
        transactionTypeLabel.text = transaction.transactionType.name()
        amountLabel.text = transaction.amount.convertToSaldo()
        balanceLabel.text = transaction.balance.convertToSaldo()
        desintationNameLabel.text = transaction.destinationName
        desintationAccountLabel.text = transaction.destinationAccount

        val isTransfer: Boolean = transaction.transactionType.equals(TransactionType.TRANSFER)
        desintationAccountLabel.isVisible = isTransfer
        desintationNameLabel.isVisible = isTransfer
        desintationAccountLabelPrompt.isVisible = isTransfer
        desintationNameLabelPrompt.isVisible = isTransfer

    }

}
