package app.gui.dashboard

import app.gui.utils.convertToSaldo
import app.network.NetworkClient
import bds.wsdl.*
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.math.BigDecimal

/**
 * ViewModel for the DashboardController
 */
@Component
open class DashboardViewModel {
    @Autowired private lateinit var networkClient: NetworkClient

    lateinit var client: Client
    /**
     * Observable list of Account objects
     */
    var accountsObservableList: ObservableList<Account> = FXCollections.observableList(mutableListOf())
    /**
     * Observable list of Transaction objects
     */
    val transactionsObservable: ObservableList<Transaction?> = FXCollections.observableList(mutableListOf())
    /**
     * Observable string of the saldo
     */
    var saldoObservable: StringProperty = SimpleStringProperty()

    /**
     * Select account and perform update
     */
    fun selectAccount(sourceAccount: String) {
        val transactions = networkClient.getTransactions(sourceAccount).reversed()
        transactionsObservable.setAll(transactions)
        saldoObservable.value = accountsObservableList.first { it.accountNumber.equals(sourceAccount) }.balance.convertToSaldo()
    }

    /**
     * Add new transaction
     * @param sourceAccount account
     * @param transaction new Transaction
     */
    fun addTransaction(sourceAccount: String, transaction: Transaction) {
        transactionsObservable.add(0, transaction)
        saldoObservable.value = transaction.balance.convertToSaldo()
    }

    /**
     * Start with the client
     * @param client selected client
     */
    fun start(client: Client) {
        this.client = client
        accountsObservableList.setAll(networkClient.getAccounts(client.id))
    }

    /**
     * Add new account
     */
    fun addAccount(): Account {
        val account = networkClient.addAccount(client.id)
        accountsObservableList.add(account)
        return account
    }

    /**
     * Refesh action
     * @param sourceAccount account to refresh
     */
    fun refresh(sourceAccount: String) {
        selectAccount(sourceAccount)
    }

    /**
     * Perform transaction
     * @param transactionType type of the transaction
     * @param title title of the transaction
     * @param destinationAccount destination account (receiver name)
     * @param destinationName destination name
     * @param amount amount
     * @param sourceName source name (sender name)
     * @param sourceAccount source account
     */
    fun performTransaction(transactionType: TransactionType, title: String?, destinationAccount: String?,
                           destinationName: String?, amount: Int, sourceName: String?, sourceAccount: String?) {

        val cashModel = CashModel()
        cashModel.amount = amount
        cashModel.sourceName = sourceName
        cashModel.title = title
        cashModel.sourceAccount = sourceAccount

        when(transactionType) {
            TransactionType.TOP_UP -> addTransaction(cashModel.sourceAccount, networkClient.topUp(cashModel = cashModel))
            TransactionType.CASH_WITHDRAWAL -> addTransaction(cashModel.sourceAccount, networkClient.cashWithdrawal(cashModel = cashModel))
            TransactionType.TRANSFER -> {
                val transferModel = TransferModel()
                transferModel.destinationAccount = destinationAccount
                transferModel.destinationName = destinationName
                addTransaction(cashModel.sourceAccount, networkClient.transfer(transferModel = transferModel))
            }
        }
    }


}

