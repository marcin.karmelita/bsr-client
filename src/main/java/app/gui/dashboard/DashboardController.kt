package app.gui.dashboard

import app.gui.config.SceneType
import app.gui.dashboard.history.HistoryItemController
import app.gui.login.LoginController
import app.gui.utils.AccountNumbersProvider
import app.gui.utils.StringAccountConverter
import app.gui.utils.TransactionTypeConverter
import app.gui.utils.name
import bds.wsdl.*
import javafx.collections.FXCollections
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.IOException
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.ListCell
import javafx.stage.Stage
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.ws.soap.client.SoapFaultClientException
import java.math.BigDecimal

/**
 * Dashboard controller
 */
@Component
open class DashboardController {
    val SUCCESS = "Success"

    @FXML lateinit var logoutButton: Button
    @FXML lateinit var refreshButton: Button
    @FXML lateinit var addAccountButton: Button
    @FXML lateinit var accounts: ChoiceBox<Account>
    @FXML lateinit var saldoLabel: Label
    @FXML lateinit var transactionTypeChoiceBox: ChoiceBox<TransactionType>
    @FXML lateinit var accountListComboBox: ComboBox<String>
    @FXML lateinit var amountTextField: TextField
    @FXML lateinit var destinationNameTextField: TextField
    @FXML lateinit var submitTransactionButton: Button
    @FXML lateinit var titleTextField: TextField
    @FXML lateinit var errorLabel: Label
    @FXML lateinit var history: ListView<Transaction?>
    @FXML lateinit var sourceNameTextField: TextField

    @Autowired private lateinit var applicationContext: ConfigurableApplicationContext
    @Autowired private lateinit var viewModel: DashboardViewModel
    @Autowired private lateinit var accountNumbersProvider: AccountNumbersProvider

    /**
     * Initialize view
     */
    fun initialize() {
        prepareAccountsComponent()
        prepareTransactionsComponent()
        prepareHistoryComponent()

    }

    private fun prepareHistoryComponent() {
        history.setCellFactory({ lv ->
            object : ListCell<Transaction?>() {

                private var controller: HistoryItemController? = null
                private var graphics: Node
                init {
                    try {

                        val fxmlLoader = FXMLLoader(javaClass.classLoader.getResource(SceneType.HISTORY_ITEM.value))
                        graphics = fxmlLoader.load<Any>() as Node
                        controller = fxmlLoader.getController<Any>() as HistoryItemController?
                    } catch (exc: IOException) {
                        throw RuntimeException(exc)
                    }
                }

                override fun updateItem(transaction: Transaction?, empty: Boolean) {
                    super.updateItem(transaction, empty)
                    if (empty) {
                        graphic = null
                    } else {
                        graphic = graphics
                        transaction?.let { controller?.setup(it) }
                    }
                }
            }
        })
    }


    private fun prepareTransactionsComponent() {
        transactionTypeChoiceBox.items = FXCollections.observableList(listOf(TransactionType.TRANSFER,
                TransactionType.CASH_WITHDRAWAL, TransactionType.TOP_UP))
        transactionTypeChoiceBox.selectionModel.select(0)
        transactionTypeChoiceBox.converter = TransactionTypeConverter()

        transactionTypeChoiceBox.selectionModel.selectedItemProperty().addListener { _, _, newValue ->
            when (newValue) {
                TransactionType.TRANSFER -> {
                    accountListComboBox.isDisable = false
                    destinationNameTextField.isDisable = false
                    sourceNameTextField.isDisable = false
                    sourceNameTextField.text = ""
                } else -> {
                accountListComboBox.isDisable = true
                destinationNameTextField.isDisable = true
                sourceNameTextField.isDisable = true
                sourceNameTextField.text = "Me"
                accountListComboBox.editor.text = ""
            }
            }
            titleTextField.text = newValue.name()
        }
    }

    private fun prepareAccountsComponent() {
        accounts.selectionModel.selectedItemProperty().addListener { _, _, newValue ->
            println(newValue.accountNumber)
            viewModel.selectAccount(newValue.accountNumber)
        }

        accounts.converter = StringAccountConverter()
        accounts.itemsProperty().addListener { _, _, _ ->
            accounts.layout()
        }
    }

    /**
     * Start controller with the Client resource
     * @param client current client
     */
    internal fun start(client: Client) {
        viewModel.start(client)

        history.items = viewModel.transactionsObservable
        viewModel.saldoObservable.addListener { _, _, newValue ->
            saldoLabel.text = newValue
        }
        // update view
        updateView()
    }

    private fun updateView() {
        accountListComboBox.items = FXCollections.observableList(accountNumbersProvider.provide())
        accounts.items = viewModel.accountsObservableList

        accounts.selectionModel.takeIf { accounts.items.isNotEmpty() }?.select(0)
    }

    /**
     * Logout handler
     */
    @FXML protected fun handleLogoutAction(event: ActionEvent) {
        val login = Login()

        val fxmlLoader = FXMLLoader(javaClass.classLoader.getResource(SceneType.LOGIN.value))
        fxmlLoader.setControllerFactory { applicationContext.getBean(it) }
        val root: Parent = fxmlLoader.load()
        val scene = Scene(root, 800.0, 600.0)
        val appStage = (event.source as Node).scene.window as Stage
        appStage.scene = scene
        appStage.show()
        val controller = fxmlLoader.getController<LoginController>()
    }

    /**
     * Refresh handler
     */
    @FXML protected fun handleRefreshAction(event: ActionEvent) {
        viewModel.refresh(accounts.selectionModel.selectedItem.accountNumber)
    }

    /**
     * Add account handler
     */
    @FXML protected fun handleAddAccountAction(event: ActionEvent) {
        try {
            accounts.selectionModel.select(viewModel.addAccount())
            errorLabel.text = SUCCESS
        } catch (e: SoapFaultClientException) {
            errorLabel.text = e.localizedMessage
        }
    }

    /**
     * Transaction handler
     */
    @FXML protected fun handleTransactionAction(event: ActionEvent) {
        try {
            when (amountTextField.text.matches("^[1-9][0-9]*.[0-9]{2}\$".toRegex())) {
                false -> { errorLabel.text = "Invalid form."
                }
                else -> {
                    viewModel.performTransaction(transactionType = transactionTypeChoiceBox.value,
                            title = titleTextField.text,
                            destinationAccount = accountListComboBox.value,
                            destinationName = destinationNameTextField.text,
                            amount = amountTextField.text.toBigDecimal().multiply(BigDecimal(100)).intValueExact(),
                            sourceName = sourceNameTextField.text,
                            sourceAccount = accounts.selectionModel.selectedItem.accountNumber)
                    errorLabel.text = SUCCESS
                }
            }
        } catch (e: Exception) {
            errorLabel.text = e.message
        }
    }
}

