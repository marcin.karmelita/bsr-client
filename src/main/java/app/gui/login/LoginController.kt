package app.gui.login

import app.gui.config.SceneType
import app.gui.dashboard.DashboardController
import app.network.NetworkClient
import bds.wsdl.Client
import bds.wsdl.Login
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.text.Text
import javafx.stage.Stage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.stereotype.Component

/**
 * Login controller
 */
@Component
class LoginController {
    @FXML lateinit var loginField: TextField
    @FXML lateinit var passwordField: TextField
    @FXML lateinit var loginButton: Button
    @FXML lateinit var registerButton: Button
    @FXML lateinit var errorMessage: Text

    @Autowired private lateinit var networkClient: NetworkClient
    @Autowired private lateinit var applicationContext: ConfigurableApplicationContext

    /**
     * Login handler
     */
    @FXML protected fun handleLoginButtonAction(event: ActionEvent) {
        try {
            val login = Login()
            login.login = loginField.text
            login.password = passwordField.text
            val client = networkClient.login(login)
            showClient(event, client)
        } catch (e: Exception) {
            e.printStackTrace()
            errorMessage.text = e.message
        }
    }

    /**
     * Registraction handler
     */
    @FXML protected fun handleRegisterButtonAction(event: ActionEvent) {
        try {
            val login = Login()
            login.login = loginField.text
            login.password = passwordField.text
            val client = networkClient.register(login)
            showClient(event, client)
        } catch (e: Exception) {
            e.printStackTrace()
            errorMessage.text = e.message
        }
    }

    private fun showClient(event: ActionEvent, client: Client) {
        val fxmlLoader = FXMLLoader(javaClass.classLoader.getResource(SceneType.DASHBOARD.value))
        fxmlLoader.setControllerFactory { applicationContext.getBean(it) }
        val root: Parent = fxmlLoader.load()
        val scene = Scene(root, 800.0, 600.0)
        val appStage = (event.source as Node).scene.window as Stage
        appStage.scene = scene
        appStage.show()
        val controller = fxmlLoader.getController<DashboardController>()
        controller.start(client = client)
    }
}
