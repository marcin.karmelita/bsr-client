package app.gui.config

/**
 * Enum class containing all the scenes
 */
enum class SceneType(val value: String) {
    DASHBOARD("GUI.fxml"),
    LOGIN("Login.fxml"),
    HISTORY_ITEM("HistoryItem.fxml");
}