package app.gui.utils

import org.springframework.stereotype.Component
import java.io.File

/**
 * Read known accounts from file
 */
@Component
open class AccountNumbersProvider {

    /**
     * Provide known accounts
     * @param filename filename
     * @param filter closure for filtering resources
     * @return List of account numbers
     */
    fun provide(filename: String = "/Users/marcinkarmelita/IdeaProjects/bdsclient/src/main/resources/account.csv",
                filter: (String) -> Boolean = { _ -> true }): List<String> {
        val file = File(filename)
        return file
                .readLines()
                .map { line -> line.replace("\\s".toRegex(), "") }
                .filter { line -> line.matches(Regex("^[0-9]{26}\$")) }
                .filter(filter)
    }
}