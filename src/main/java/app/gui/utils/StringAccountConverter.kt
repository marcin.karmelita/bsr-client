package app.gui.utils

import bds.wsdl.Account
import bds.wsdl.TransactionType
import javafx.util.StringConverter

/**
 * String Account Converter
 */
open class StringAccountConverter: StringConverter<Account>() {

    /**
     * Convert Account to String
     * @param `object` account
     * @return account converted to String
     */
    override fun toString(`object`: Account?): String {
        return `object`?.accountNumber ?: "---"
    }

    /**
     * Convert String to Account
     * @param string account number
     * @return account
     */
    override fun fromString(string: String?): Account {
        val account = Account()
        account.accountNumber = string
        return account
    }
}

/**
 * Transaction Converter
 */
open class TransactionTypeConverter: StringConverter<TransactionType>() {

    /**
     * Convert TransactionType to String
     * @param `object` transactionType
     * @return TransactionType converted to String
     */
    override fun toString(`object`: TransactionType?): String {
        return `object`?.name() ?: "---"
    }

    /**
     * Convert TransactionType to Account
     * @param string transactionType
     * @return String converted to TransactionType
     */
    override fun fromString(string: String?): TransactionType {
        return TransactionType.fromValue(string)
    }
}
