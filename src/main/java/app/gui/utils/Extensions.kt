package app.gui.utils

import bds.wsdl.TransactionType
import java.math.BigDecimal

/**
 * Extension for getting the name for the selected transaction option
 */
fun TransactionType.name(): String {
    return when (this) {
        TransactionType.TOP_UP -> "Top up"
        TransactionType.CASH_WITHDRAWAL -> "Cash withdrawal"
        TransactionType.TRANSFER -> "Transfer"
    }
}

/**
 * Extension for converting Int to saldo formatted String
 */
fun Int.convertToSaldo(): String = "%.2f PLN".format(this.toBigDecimal().div(BigDecimal(100)))
